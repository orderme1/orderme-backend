package com.restaurant.order.restaurantOrder.controllers;

import com.restaurant.order.restaurantOrder.model.UserDto;
import com.restaurant.order.restaurantOrder.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/users")
@Slf4j
public class AuthController {

    private UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/registration", consumes = "application/json")
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto) {
        var newUserDto = userService.createUser(userDto);
        return new ResponseEntity<>(newUserDto, HttpStatus.CREATED);
    }
}
