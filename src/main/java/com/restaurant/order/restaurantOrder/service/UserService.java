package com.restaurant.order.restaurantOrder.service;

import com.restaurant.order.restaurantOrder.model.UserDto;
import com.restaurant.order.restaurantOrder.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public UserDto createUser(UserDto userDto) {

        var userEntity = userRepository.save(userDto.toUser());
        return UserDto.from(userEntity);
    }
}
