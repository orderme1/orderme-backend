package com.restaurant.order.restaurantOrder.repository;

import com.restaurant.order.restaurantOrder.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
