package com.restaurant.order.restaurantOrder.model;

import com.restaurant.order.restaurantOrder.entity.User;

public record UserDto(
        long id,
        String firstName,
        String middleName,
        String lastName,
        String email,
        String phoneNumber,
        String role
) {
    public static UserDto from(User userEntity) {
        return new UserDto(
                userEntity.getId(),
                userEntity.getFirstName(),
                userEntity.getMiddleName(),
                userEntity.getLastName(),
                userEntity.getEmail(),
                userEntity.getPhoneNumber(),
                userEntity.getRole()
                );
    }

    public User toUser() {
        return new User(id, firstName, middleName,lastName, email, phoneNumber, role);
    }

}
